Illumina_CordBlood 
-------------

This git repository contains a package with 450k reference data on cord blood, provided by Kristina Gervin and colleagues.

Package releases
----------------

Source tarballs for R, built using R-devel. This has been submitted to Bioconductor.

- [FlowSorted.CordBloodNorway.450k_0.1.0.tar.gz](https://dl.dropboxusercontent.com/u/3447867/FlowSorted.CordBlood.450k_0.0.1.tar.gz))


To do 
-----

- CITATION (when ready)
- When IDAT files have been submitted to GEO, supply datasheet
- Test code for cell type proportion estimation

Old releases
------------

- [FlowSorted.CordBlood.450k_0.0.1.tar.gz](https://dl.dropboxusercontent.com/u/3447867/FlowSorted.CordBlood.450k_0.0.1.tar.gz)
