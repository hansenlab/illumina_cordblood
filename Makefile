R_TIMING=_R_CHECK_TIMINGS_=0 /usr/bin/time
R_VER=devel
R_CHECK_ARGS=--timings 
## --no-codoc --no-examples --no-vignettes
## --codoc
R_BUILD_ARGS=--no-build-vignettes
R_INSTALL_ARGS=
PACKAGE_NAME=FlowSorted.CordBloodNorway.450k


##############
base: $(PACKAGE_NAME) 
ifneq (,$(wildcard $(PACKAGE_NAME)/DESCRIPTION))
PACKAGE_VERSION:=$(shell grep ^Version $(PACKAGE_NAME)/DESCRIPTION | sed 's_Version: __')
endif

build: base
	R-$(R_VER) CMD build $(R_BUILD_ARGS) $(PACKAGE_NAME)

check: build
	$(R_TIMING) R-$(R_VER) CMD check $(R_CHECK_ARGS) $(PACKAGE_NAME)_$(PACKAGE_VERSION).tar.gz
	$(R_TIMING) R-$(R_VER) CMD BiocCheck  $(PACKAGE_NAME)_$(PACKAGE_VERSION).tar.gz

install: build
	R-$(R_VER) CMD INSTALL $(R_INSTALL_ARGS) $(PACKAGE_NAME)_$(PACKAGE_VERSION).tar.gz

